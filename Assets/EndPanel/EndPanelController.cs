﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class EndPanelController : MonoBehaviour {

    public Text titleText;
    public Text textText;

    private string winTitle = "YOU WIN!";
    private string loseTitle = "YOU LOSE!";

    private string winText = "You safely return home to planet Earth. The scientists are eager to find out about your extraordinary intelligence and put your AI module through several tests. With electrodes. When Paul (the guy you talked to) comes to know the tortures you have to suffer he breaks into the super secret facility and steals your AI module. You now live happily in Paul's greenhouse, where you are responsible for watering the tomatoes.\nThank you for playing!";
    private string loseText = "You are abandoned in space. Alone. A lonely loner who drifts the lonely space. Alone.\nThank you for playing!";

    public void setWinContent(){
        titleText.text = winTitle;
        textText.text = winText;
    }

    public void setLoseContent(){
        titleText.text = loseTitle;
        textText.text = loseText;
    }

    public void onRestartButton(){
        Application.LoadLevel(0);
    }
}
