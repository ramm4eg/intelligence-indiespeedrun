﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using ButtonList.intern;


namespace ButtonList
{
    public class ButtonListController : MonoBehaviour, IButtonListClickListener
    {

        public GameObject buttonList;

        private InternalButtonListController internalController;

        private List<IButtonListClickListener> clickListenerList;

        public void addClickListener(IButtonListClickListener listener)
        {
            if (clickListenerList == null)
            {
                clickListenerList = new List<IButtonListClickListener>();
            }
            clickListenerList.Add(listener);
        }

        public void removeClickListener(IButtonListClickListener listener)
        {
            if (clickListenerList != null)
            {
                clickListenerList.Remove(listener);
            }
        }


        public void Start()
        {

            // Get internal button controller from game object

            GameObject viewPort = buttonList.transform.GetChild(0).gameObject;
            internalController = viewPort.GetComponent<InternalButtonListController>();
            internalController.addClickListener(this);

        }


        public void addButton(string bText)
        {
            internalController.addButton(bText);
        }

        public void removeAllButtons()
        {
            List<Button> buttons = internalController.getButtons();
            foreach(Button b in buttons){
                Destroy(b.gameObject);
            }
            buttons.Clear();
        }

        public void buttonClicked(string name)
        {
            // Debug.Log("Controller: " + name);
            if (clickListenerList != null)
            {
                foreach (IButtonListClickListener listener in clickListenerList)
                {
                    listener.buttonClicked(name);
                }

            }
        }

        


    }

}
