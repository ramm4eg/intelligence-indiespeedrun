﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

namespace ButtonList.intern
{
    public class InternalButtonListController : MonoBehaviour
    {
        public Button buttonPrefab;
        public GameObject rowPrefab;

        public GameObject viewPort;
        public GameObject scrollContent;

        private List<IButtonListClickListener> clickListenerList;

        private List<Button> buttonList = new List<Button>();

        public void addClickListener(IButtonListClickListener listener)
        {
            if (clickListenerList == null)
            {
                clickListenerList = new List<IButtonListClickListener>();
            }
            clickListenerList.Add(listener);
        }

        public void removeClickListener(IButtonListClickListener listener)
        {
            if (clickListenerList != null)
            {
                clickListenerList.Remove(listener);
            }
        }

        private void setInvisible(Button b)
        {

            Fader bFader = b.GetComponent<Fader>();
            bFader.FadeOut(b.gameObject);
            
        }
        
        private void fadeInButton(Button b)
        {
            Fader bFader = b.GetComponent<Fader>();
            bFader.FadeIn(b.gameObject);
        }

        public List<Button> getButtons()
        {
            return buttonList;
        }


        public void addButton(string buttonText)
        {

            // Get width of View Port
            RectTransform rt = viewPort.GetComponent<RectTransform>();
            float maxWidth = rt.rect.width - 50;
            // Debug.Log("Max: " + maxWidth);

            // Create a new button with given text and calculate prefferred width of button
            Button b = GameObject.Instantiate(buttonPrefab) as Button;
            buttonList.Add(b);
            
            b.onClick.AddListener(() => { buttonClicked(buttonText); });
            b.transform.GetComponentInChildren<Text>().text = buttonText;
            float prefWidth = b.transform.GetComponentInChildren<Text>().preferredWidth;
            // Debug.Log("Button Pref: " + prefWidth);

            // Find row element to add button to
            bool wasAdded = false;
            for (int i = 0; i < scrollContent.transform.childCount; i++)
            {
                Transform row = scrollContent.transform.GetChild(i);

                HorizontalLayoutGroup lg = row.GetComponent<HorizontalLayoutGroup>();
                float rowWidth = lg.preferredWidth;
                // Debug.Log("Row: " + rowWidth);

                if (maxWidth - (rowWidth + prefWidth) > 0)
                {
                    //Debug.Log("Adding");
                    wasAdded = true;
                    b.transform.SetParent(lg.transform);
                    break;
                }
                else
                {
                    //    Debug.Log("Not Added");
                }

            }

            if (!wasAdded)
            {
                VerticalLayoutGroup vg = scrollContent.GetComponent<VerticalLayoutGroup>();
                //Debug.Log("Creating new row");
                GameObject nuRow = GameObject.Instantiate(rowPrefab);

                RectTransform cRec = scrollContent.GetComponent<RectTransform>();
                Vector2 oldSize = cRec.sizeDelta;
                Vector2 nuSize = new Vector2(oldSize.x, oldSize.y + 30);
                cRec.sizeDelta = nuSize;

                nuRow.transform.SetParent(vg.transform);



                HorizontalLayoutGroup lg = nuRow.GetComponent<HorizontalLayoutGroup>();
                b.transform.SetParent(lg.transform);
            }

            fadeInButton(b);
            //setInvisible(b);


        }

        private void buttonClicked(string name)
        {

            //Debug.Log(name);
            if (clickListenerList != null)
            {
                foreach (IButtonListClickListener listener in clickListenerList)
                {
                    listener.buttonClicked(name);
                }

            }

        }




    }

}
