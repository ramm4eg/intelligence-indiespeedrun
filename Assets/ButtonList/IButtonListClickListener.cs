﻿using System;
using System.Text;

namespace ButtonList
{
    public interface IButtonListClickListener
    {

        void buttonClicked(string name);

    }

}
