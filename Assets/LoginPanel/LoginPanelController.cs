﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using ButtonList;

public class LoginPanelController : MonoBehaviour
{

    public Text textField;
    IButtonListClickListener clickHandler;

    public void setClickHandler(IButtonListClickListener handler)
    {
        clickHandler = handler;
    }

    public void onButtonClick()
    {
        string name = textField.text;
        if (name.Length == 0)
        {
            name = "TooCoolForAName";
        }
        if (clickHandler != null)
        {
            clickHandler.buttonClicked(name);
        }

    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

}
