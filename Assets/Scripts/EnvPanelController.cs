﻿using UnityEngine;
using System.Collections;

public class EnvPanelController : BasePanelController
{

    public new void buttonClicked(string name)
    {
        Debug.Log("Environment Panel: " + name);
        base.buttonClicked(name);
    }
}
