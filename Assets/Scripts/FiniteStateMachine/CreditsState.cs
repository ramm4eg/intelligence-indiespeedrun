﻿using System;
using System.Collections.Generic;

internal class CreditsState : IntGameState, GameManager.CommandListener
{
    private bool _startCommandExecuted = false;
    WordPanelController wordPanelController;

    void IntGameState.OnEnter()
    {
        // write the texts
        GameStateMachine.consoleController.TypeLine("");
        GameStateMachine.consoleController.TypeLine("Intelligence");
        GameStateMachine.consoleController.TypeLine("");
        GameStateMachine.consoleController.TypeLine("This game was developed as part of Indie Speed Run 2015 (www.indiespeedrun.com).");
        for (int i = 0; i < 3; i++)
        {
            GameStateMachine.consoleController.TypeLine("");
        }
        GameStateMachine.consoleController.TypeLine("Design and programming by David Hamjediers and Misha Shlyakhtovskyy");
        GameStateMachine.consoleController.TypeLine("");
        GameStateMachine.consoleController.TypeLine("(c) RSF 2015");
        GameStateMachine.consoleController.TypeLine("");
        GameStateMachine.consoleController.TypeLine("");
        GameStateMachine.consoleController.TypeLine("");
        GameStateMachine.consoleController.TypeLine("To start interacting with the game, please select a command in the upper right corner of the screen, click on it and press Execute button on the bottom.");

        // init the command panel
        GameManager.Instance.addCommand(WordFactory.getWord("start_game") as Command);
        GameManager.Instance.addCommand(WordFactory.getWord("controls") as Command);
        GameManager.Instance.addCommand(WordFactory.getWord("description") as Command);
        GameManager.Instance.addCommand(WordFactory.getWord("thanks") as Command);
        wordPanelController = GameManager.Instance.GetComponent<WordPanelController>();
        wordPanelController.showCommandPanel();

        // wait for an event
        GameManager.Instance.addCommandListener(this);
    }

    IntGameState IntGameState.Update()
    {
        if (_startCommandExecuted)
        {
            GameManager.Instance.removeCommandListener(this);
            wordPanelController.clearCommands();
            return new InitState();
        }
        return null;
    }

    public void commandExecuted(LinkedList<Word> line)
    {
       LinkedListNode<Word> first = line.First;
        if (first != null && first.Value is StartCommand)
        {
            _startCommandExecuted = true;
        }
    }
}