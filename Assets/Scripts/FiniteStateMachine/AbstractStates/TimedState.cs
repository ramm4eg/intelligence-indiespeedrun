﻿using System;
using UnityEngine;

internal abstract class TimedState : IntGameState
{
    protected float timer = 0f;
    private IntGameState _nextState;
    private float _timeout;

    protected void setTimeOut(float timeOut) {
        _timeout = timeOut;
    }

    protected void setNextState(IntGameState nextState) { 
        _nextState = nextState;
    }

    private IntGameState countdown()
    {
        timer += Time.deltaTime;
        if (timer >= _timeout)
        {
            return _nextState;
        }
        return null;
    }

    public abstract void OnEnter();

    public IntGameState Update()
    {
        if (GameStateMachine.consoleController.isTyping())
        {
            // don't start countdown before the typing ends
            return null;
        }
        return countdown();
    }
}