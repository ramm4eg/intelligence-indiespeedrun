﻿internal interface IntGameState
{
    IntGameState Update();
    void OnEnter();
}