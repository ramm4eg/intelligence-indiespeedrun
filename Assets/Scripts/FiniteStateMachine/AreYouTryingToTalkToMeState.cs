﻿using System.Collections.Generic;
using UnityEngine;

internal class AreYouTryingToTalkToMeState : IntGameState, GameManager.CommandListener
{
    private bool _commandWithMessageSent = false;
    Command sendDataCommand = WordFactory.getWord("send_data") as Command;
    Command sendMessageCommand = WordFactory.getWord("send_message") as Command;

    public void commandExecuted(LinkedList<Word> line)
    {
        if (line.First == null || (line.First.Value != sendDataCommand && line.First.Value != sendMessageCommand))
        {
            return;
        }
        else
        {
            LinkedListNode<Word> second = line.First.Next;
            if (second == null)
            {
                int roll = Random.Range(0, 3);
                switch (roll)
                {
                    case 0:
                        GameManager.Instance.sendResponceMessage("Say 'I robot'!");
                        GameManager.Instance.LearnWord("I");
                        GameManager.Instance.LearnWord("robot");
                        break;
                    case 1:
                        GameManager.Instance.sendResponceMessage("Come on, tell me something else!");
                        GameManager.Instance.LearnWord("come");
                        break;
                    case 2:
                        GameManager.Instance.sendResponceMessage("Hey, are you there?!");
                        GameManager.Instance.LearnWord("hey");
                        break;
                    default:
                        break;
                }

            }
            else
            {
                if (second.Value is EnvObject && second.Next == null)
                {
                    GameManager.Instance.sendResponceMessage("Thank you!");
                }
                else
                {
                    GameManager.Instance.sendResponceMessage("Cool! It seems you are learning to talk! Tell me about yourself!");
                    GameManager.Instance.LearnWord("cool");
                    GameManager.Instance.LearnWord("learning");
                    GameManager.Instance.LearnWord("talk");
                    _commandWithMessageSent = true;
                }
            }
        }
    }

    public void OnEnter()
    {
        GameManager.Instance.addCommandListener(this);
    }

    public IntGameState Update()
    {
        if (_commandWithMessageSent)
        {
            GameManager.Instance.removeCommandListener(this);
            return new TalkingSentencesState();
        }

        return null;
    }
}