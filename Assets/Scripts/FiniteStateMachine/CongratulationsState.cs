﻿internal class CongratulationsState : IntGameState
{
    private IntGameState _state;

    public void OnEnter()
    {
        _state = new WaitFewSeconds();
        _state.OnEnter();
    }

    public IntGameState Update()
    {
        var newState = _state.Update();
        if (newState != null)
        {
            if (newState == GameStateMachine.END_STATE)
            {
                return GameStateMachine.END_STATE;
            }
            else
            {
                _state = newState;
                _state.OnEnter();
            }
        }
        return null;
    }
}