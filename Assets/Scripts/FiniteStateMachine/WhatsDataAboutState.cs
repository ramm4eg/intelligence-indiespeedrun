﻿using System.Collections.Generic;
using UnityEngine;

internal class WhatsDataAboutState : IntGameState, GameManager.CommandListener
{
    private bool _commandWithMessageSent = false;
    private int _failedCommunicationCounter = 0;
    Command sendDataCommand = WordFactory.getWord("send_data") as Command;
    Command sendMessageCommand = WordFactory.getWord("send_message") as Command;

    public void commandExecuted(LinkedList<Word> line)
    {
        if (line.First == null || (line.First.Value != sendDataCommand && line.First.Value != sendMessageCommand))
        {
            return;
        }
        else
        {
            LinkedListNode<Word> second = line.First.Next;
            if (second == null)
            {
                int roll = Random.Range(0,2);
                if (_failedCommunicationCounter > 3 || roll == 0 )
                {
                    GameManager.Instance.sendResponceMessage("<Empty>");
                } else { 
                _failedCommunicationCounter++;
                    switch (_failedCommunicationCounter)
                    {
                        case 1:
                            GameManager.Instance.sendResponceMessage("It's so frustrating to sort this data... if you could only send me the object it's depicting...");
                            GameManager.Instance.LearnWord("data");
                            GameManager.Instance.LearnWord("send");
                            GameManager.Instance.LearnWord("me");
                            break;
                        case 2:
                            GameManager.Instance.sendResponceMessage("Once again no description... It's silly to get mad at a machine, but I'm still mad at you!");
                            GameManager.Instance.LearnWord("machine");
                            GameManager.Instance.LearnWord("silly");
                            break;
                        case 3:
                            GameManager.Instance.sendResponceMessage("Sitting here... lonely, tired and frustrated... talking to a stupid machine a billion miles away... I hate my life...");
                            GameManager.Instance.LearnWord("lonely");
                            GameManager.Instance.LearnWord("tired");
                            GameManager.Instance.LearnWord("stupid");
                            GameManager.Instance.LearnWord("machine");
                            GameManager.Instance.LearnWord("life");
                            break;
                        default:
                            break;
                    }
                }
            }
            else
            {
                if (second.Value is EnvObject)
                {
                    GameManager.Instance.sendResponceMessage("You can talk! Thanks for your answer!");
                    GameManager.Instance.LearnWord("thanks");
                    GameManager.Instance.LearnWord("answer");
                }
                else
                {
                    GameManager.Instance.sendResponceMessage("Are you trying to tell me something?");
                }
                _commandWithMessageSent = true;
            }
        }
    }

    public void OnEnter()
    {
        GameManager.Instance.addCommandListener(this);
    }

    public IntGameState Update()
    {
        if (_commandWithMessageSent)
        {
            GameManager.Instance.removeCommandListener(this);
            return new AreYouTryingToTalkToMeState();
        }

        return null;
    }
}