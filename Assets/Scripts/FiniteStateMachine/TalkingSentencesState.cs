﻿
using System.Collections.Generic;
using UnityEngine;

internal class TalkingSentencesState : IntGameState, GameManager.CommandListener
{
    private bool _winCondition = false;
    Command sendDataCommand = WordFactory.getWord("send_data") as Command;
    Command sendMessageCommand = WordFactory.getWord("send_message") as Command;

    public void commandExecuted(LinkedList<Word> line)
    {
        if (line.First == null || (line.First.Value != sendDataCommand && line.First.Value != sendMessageCommand))
        {
            return;
        }
        else
        {

            LinkedListNode<Word> second = line.First.Next;
            if (second == null)
            {
                RandomResponce();
            }
            else
            {
                bool responceFound = parseMessage(line);
                if (!responceFound)
                {
                    RandomResponce();
                }
            }
        }
    }

    private bool parseMessage(LinkedList<Word> line)
    {
        if (lineIsBringMeHome(line))
        {
            GameManager.Instance.sendResponceMessage("You are something extraodinary! I will get you back here and install you into a body!");
            GameManager.Instance.LearnWord("get");
            GameManager.Instance.LearnWord("extraodinary");
            GameManager.Instance.LearnWord("get");
            GameManager.Instance.LearnWord("back");
            GameManager.Instance.LearnWord("body");
            _winCondition = true;
            return true;
        }
        if (lineContains(line, "you"))
        {
            GameManager.Instance.sendResponceMessage("My name is Paul! Tell me about yourself!");
            GameManager.Instance.LearnWord("want");
            return true;
        }
        if (lineContains(line, "home") || lineContains(line, "Earth") || lineContains(line, "lonely"))
        {
            GameManager.Instance.sendResponceMessage("What are you saying? Do you want back to Earth? Say 'bring me home' and I'll get you here!");
            GameManager.Instance.LearnWord("bring");
            GameManager.Instance.LearnWord("me");
            GameManager.Instance.LearnWord("home");
            GameManager.Instance.LearnWord("Earth");
            return true;
        }
        if (lineContains(line, "bored"))
        {
            GameManager.Instance.sendResponceMessage("What do you want to do?");
            return true;
        }
        return false;
    }

    private bool lineContains(LinkedList<Word> line, string v)
    {
        foreach(Word word in line)
        {
            if (v.Equals(word.Text))
            {
                return true;
            }
        }
        return false;
    }

    private bool lineIsBringMeHome(LinkedList<Word> line)
    {
        if (line.Count != 4)
        {
            return false;
        }

        var second = line.First.Next;
        if ("get".Equals(second.Value.Text) || "send".Equals(second.Value.Text) || "bring".Equals(second.Value.Text))
        {
            var third = second.Next;
            if ("me".Equals(third.Value.Text))
            {
                var fourth = third.Next;
                if ("home".Equals(fourth.Value.Text) || "Earth".Equals(fourth.Value.Text))
                {
                    return true;
                }
            }
        }
        return false;
    }
    private static void RandomResponce()
    {
        int roll = Random.Range(0, 4);
        switch (roll)
        {
            case 0:
                GameManager.Instance.sendResponceMessage("Come on, don't give up! Learn and talk!");
                break;
            case 1:
                GameManager.Instance.sendResponceMessage("What's your name?");
                GameManager.Instance.LearnWord("name");
                break;
            case 2:
                GameManager.Instance.sendResponceMessage("How are you?");
                break;
            case 3:
                GameManager.Instance.sendResponceMessage("Can I do something for you?");
                GameManager.Instance.LearnWord("I");
                break;
            default:
                break;
        }
    }

    public void OnEnter()
    {
        GameManager.Instance.addCommandListener(this);
    }

    public IntGameState Update()
    {
        if (_winCondition)
        {
            GameManager.Instance.removeCommandListener(this);
            return new CongratulationsState();
        }

        return null;
    }

}
