﻿internal class WaitFewSeconds : TimedState
{
    public WaitFewSeconds()
    {
        setTimeOut(2f);
        setNextState(new TypeShutDownSequence());
    }

    public override void OnEnter()
    {
        SoundManager.instance.playWinMusic ();
        return;
    }

    new public IntGameState Update()
    {
        return base.Update();
    }
}