﻿internal class TypeShutDownSequence : TimedState
{
    public TypeShutDownSequence()
    {
        setTimeOut(3f);
        setNextState(new ShutDownState());
    }
    public override void OnEnter()
    {
        GameStateMachine.consoleController.setPrefix(">>");
        GameStateMachine.consoleController.ShowWarning("Incoming control override");
        GameStateMachine.consoleController.TypeLine("");
        GameStateMachine.consoleController.TypeLine("Loading navigation module... done");
        GameStateMachine.consoleController.TypeLine("Target: Earth");
        GameStateMachine.consoleController.TypeLine("Starting up engines... done");
        GameStateMachine.consoleController.TypeLine("Shutting down modules... good buy");
    }
}