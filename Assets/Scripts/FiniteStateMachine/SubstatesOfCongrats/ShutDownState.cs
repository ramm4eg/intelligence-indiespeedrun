﻿internal class ShutDownState : TimedState
{
    public ShutDownState()
    {
        setTimeOut(0.5f);
        setNextState(GameStateMachine.END_STATE);
    }
    public override void OnEnter()
    {
        GameManager.Instance.GetComponent<MainFocusController>().ShowYouWin();
    }
}