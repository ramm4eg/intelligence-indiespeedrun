﻿using System;
using UnityEngine;

internal class InitState : IntGameState
{
    private IntGameState _state;

    public void OnEnter()
    {
        _state = new TypeInitialization();
        _state.OnEnter();
        
        GameManager.Instance.addCommand(WordFactory.getWord("build") as Command);
    }

    public IntGameState Update()
    {
        var newState = _state.Update();
        if (newState != null)
        {
            if (newState == GameStateMachine.END_STATE)
            {
                return new Comm0State();
            }
            else
            {
                _state = newState;
                _state.OnEnter();
            }
        }
        return null;
    }

    
}