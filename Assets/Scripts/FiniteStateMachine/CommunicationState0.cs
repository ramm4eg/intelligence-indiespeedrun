﻿using System.Collections.Generic;
using UnityEngine;

internal class Comm0State : IntGameState, GameManager.CommandListener
{
    private bool _commandWithMessageSent = false;
    private int _failedCommunicationCounter = 0;
    Command sendDataCommand = WordFactory.getWord("send_data") as Command;
    Command sendMessageCommand = WordFactory.getWord("send_message") as Command;

    public void commandExecuted(LinkedList<Word> line)
    {
        if (line.First == null || (line.First.Value != sendDataCommand && line.First.Value != sendMessageCommand))
        {
            return;
        }
        else
        {
            LinkedListNode<Word> second = line.First.Next;
            if (second == null)
            {
                _failedCommunicationCounter++;
                GameManager.Instance.sendResponceMessage("<Empty>");
            }
            else
            {
                GameManager.Instance.sendResponceMessage("Are you trying to tell me something?");
                GameManager.Instance.LearnWord("tell");
                GameManager.Instance.LearnWord("me");
                GameManager.Instance.LearnWord("something");
                _commandWithMessageSent = true;
            }
        }
    }

    public void OnEnter()
    {
        GameManager.Instance.addCommandListener(this);
    }

    public IntGameState Update()
    {
        if (_failedCommunicationCounter >= 2)
        {
            GameManager.Instance.removeCommandListener(this);
            return new WhatsDataAboutState();
        }
        if (_commandWithMessageSent)
        {
            GameManager.Instance.removeCommandListener(this);
            return new AreYouTryingToTalkToMeState();
        }

        return null;
    }
}