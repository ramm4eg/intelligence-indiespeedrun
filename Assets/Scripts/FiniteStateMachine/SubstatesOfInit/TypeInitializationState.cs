﻿using System;

internal class TypeInitialization : TimedState
{
    public TypeInitialization()
    {
        setTimeOut(0.5f);
        setNextState(new StartMaterialsDisplayState());
    }

    public override void OnEnter()
    {
        GameStateMachine.consoleController.TypeLine("Initializing Deep Space Explorer... this may take a minute...");
        GameStateMachine.consoleController.Type("Loading Material Receiver...");
    }

    new public IntGameState Update()
    {
        return base.Update();
    }
}