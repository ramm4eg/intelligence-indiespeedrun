﻿using System;

internal class ChangePrefixState : TimedState
{
    public ChangePrefixState()
    {
        setTimeOut(0.1f);
        setNextState(GameStateMachine.END_STATE);
    }
    public override void OnEnter()
    {
        GameStateMachine.consoleController.setPrefix("" + GameManager.Instance.UserName.ToLower() + "@hubble_35.012.380>>");
        GameStateMachine.consoleController.TypeLine("");
    }
}