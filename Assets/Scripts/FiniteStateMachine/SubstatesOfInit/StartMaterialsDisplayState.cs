﻿using System;

internal class StartMaterialsDisplayState : TimedState
{
    public StartMaterialsDisplayState()
    {
        setTimeOut(1f);
        setNextState(new LoadModuleCreatorState());
    }

    public override void OnEnter()
    {
        // init materials display
        WordPanelController wpc = GameManager.Instance.GetComponent<WordPanelController>();
        wpc.showMaterialsPanel();
        GameManager.Instance.initMaterials();
    }

}