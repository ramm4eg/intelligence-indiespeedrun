﻿using System;

internal class StartSubsystemPanelState : TimedState
{
    public StartSubsystemPanelState()
    {
        setTimeOut(1f);
        setNextState(new LoadAiState());
    }

    public override void OnEnter()
    {
        WordPanelController wpc = GameManager.Instance.GetComponent<WordPanelController>();
        wpc.showSubsystemPanel();

        // init subsystem panel
        Word telescope = WordFactory.getWord("telescope");
        Word communicator = WordFactory.getWord("communicator");
        wpc.addSubsystem(telescope);
        wpc.addSubsystem(communicator);
        wpc.addSubsystem(WordFactory.getWord("ir_lens"));
        //wpc.addSubsystem(WordFactory.getWord("uv_lens"));
        //wpc.addSubsystem(WordFactory.getWord("zoom_lens"));
        wpc.addSubsystem(WordFactory.getWord("bio_probe"));
        //wpc.addSubsystem(WordFactory.getWord("magnetic_probe"));
        wpc.addSubsystem(WordFactory.getWord("communicator_2.0"));

        // init environment panel
        GameManager.Instance.GetComponent<WordPanelController>().showEnvironmentPanel();
    }
}