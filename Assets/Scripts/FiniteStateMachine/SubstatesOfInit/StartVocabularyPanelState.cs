﻿using System;

internal class StartVocabularyPanelState : TimedState
{
    public StartVocabularyPanelState()
    {
        setTimeOut(2f);
        setNextState(new PrepareLoginState());
    }

    public override void OnEnter()
    {
        // init vocabulary panel
        GameManager.Instance.GetComponent<WordPanelController>().showVocabularyPanel();
    }
}