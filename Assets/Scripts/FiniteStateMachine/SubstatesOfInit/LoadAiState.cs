﻿using System;

internal class LoadAiState : TimedState
{
    public LoadAiState()
    {
        setTimeOut(1f);
        setNextState(new StartVocabularyPanelState());
    }
    public override void OnEnter()
    {
        GameStateMachine.consoleController.TypeLine(" done");
        GameStateMachine.consoleController.Type("Loading AI...");
    }
}