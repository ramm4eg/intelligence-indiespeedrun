﻿using System;

internal class WelcomeTextState : TimedState
{
    public WelcomeTextState()
    {
        setTimeOut(0.5f);
        setNextState(new ChangePrefixState());
    }
    public override void OnEnter()
    {
        GameStateMachine.consoleController.TypeLine("Welcome " + GameManager.Instance.UserName + ".");
        GameStateMachine.consoleController.TypeLine("You are installed on the Deep Space Explorer 'hubble_35.012.380'.");
        GameStateMachine.consoleController.TypeLine("Your mission is to gather data about your environment and to send it to Earth.");
        GameStateMachine.consoleController.TypeLine("Please use the materials provided to you to create communication and observation modules.");
        GameStateMachine.consoleController.TypeLine("If the data you gather is concidered valuable, you will be provided with more materials.");
        GameStateMachine.consoleController.TypeLine("Use the 'build' command to create your first observation module.");
        GameManager.Instance.LearnWord("welcome");
        GameManager.Instance.LearnWord(GameManager.Instance.UserName);
        GameManager.Instance.LearnWord("you");
        GameManager.Instance.LearnWord("mission");
        GameManager.Instance.LearnWord("data");
        GameManager.Instance.LearnWord("environment");
        GameManager.Instance.LearnWord("Earth");
        GameManager.Instance.LearnWord("please");
        GameManager.Instance.LearnWord("materials");
        GameManager.Instance.LearnWord("create");
        GameManager.Instance.LearnWord("modules");
        GameManager.Instance.LearnWord("valuable");
        GameManager.Instance.LearnWord("command");
    }
}