﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

internal class PrepareLoginState : TimedState
{
    public PrepareLoginState()
    {
        setTimeOut(1f);
        setNextState(new LoginState());
    }

    public override void OnEnter()
    {
        GameStateMachine.consoleController.TypeLine(" please login");
    }
}
