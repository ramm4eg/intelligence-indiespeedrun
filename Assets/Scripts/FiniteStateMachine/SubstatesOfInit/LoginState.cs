﻿using System;
using ButtonList;
using UnityEngine;

internal class LoginState : IntGameState, IButtonListClickListener
{
    private bool _userNameObtained = false;

    void IntGameState.OnEnter()
    {
               
        // Deactivate Main Panel
        // show username input window, wait for input
        GameManager.Instance.GetComponent<MainFocusController>().setLoginClickHandler(this);
        GameManager.Instance.GetComponent<MainFocusController>().showLoginPanel();

    }

    IntGameState IntGameState.Update()
    {
        if (_userNameObtained)
        {
            return new WelcomeTextState();
        }
        return null;
    }

    public void buttonClicked(string userName)
    {        
        // Activate mainpanel
        GameManager.Instance.GetComponent<MainFocusController>().showMainPanel();

        GameManager.Instance.UserName = userName;
        _userNameObtained = true;
    }

}