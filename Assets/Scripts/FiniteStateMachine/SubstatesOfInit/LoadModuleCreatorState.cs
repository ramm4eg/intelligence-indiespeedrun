﻿internal class LoadModuleCreatorState : TimedState
{
    public LoadModuleCreatorState()
    {
        setTimeOut(1f);
        setNextState(new StartSubsystemPanelState());
    }
    public override void OnEnter()
    {
        GameStateMachine.consoleController.TypeLine(" done");
        GameStateMachine.consoleController.Type("Loading Module Creator...");
    }
}