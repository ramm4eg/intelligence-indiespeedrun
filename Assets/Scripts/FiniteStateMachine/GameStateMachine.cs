﻿internal class GameStateMachine
{

    internal class EndState : IntGameState
    {
        public IntGameState Update()
        {
            // do nothing
            return null;
        }

        void IntGameState.OnEnter()
        {
            return;
        }
    }


    static IntGameState _state = null;
    public static ConsoleController consoleController;
    internal static readonly IntGameState END_STATE = new EndState();

    internal static void Init(ConsoleController cc)
    {
        consoleController = cc;
        _state = new CreditsState();
        //_state = new InitState();
        _state.OnEnter();
    }

    internal static void Update()
    {
        IntGameState newState = _state.Update();
        if (newState != null)
        {
            _state = newState;
            _state.OnEnter();
        }
    }

}