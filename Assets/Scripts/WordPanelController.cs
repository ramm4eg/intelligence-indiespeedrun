﻿using UnityEngine;
using System.Collections;
using ButtonList;

public class WordPanelController : MonoBehaviour
{

    public GameObject commandPanel;
    public GameObject subsystemPanel;
    public GameObject environmentPanel;
    public GameObject vocabularyPanel;
    public GameObject materialsPanel;


    public void showCommandPanel()
    {
        showPanel(commandPanel);
    }

    public void showMaterialsPanel()
    {
        showPanel(materialsPanel);
    }

    public void showSubsystemPanel()
    {
        showPanel(subsystemPanel);
    }

    public void showEnvironmentPanel()
    {
        showPanel(environmentPanel);
    }

    public void showVocabularyPanel()
    {
        showPanel(vocabularyPanel);
    }

    private void showPanel(GameObject panel)
    {
        Fader f = panel.GetComponent<Fader>();
        f.FadeIn(panel);
    }

    public void addCommand(Word cmd)
    {
        addButton(commandPanel, cmd.Text);
    }

    public void addSubsystem(Word sys)
    {
        addButton(subsystemPanel, sys.Text);
    }

    public void addEnvironment(Word env)
    {
        addButton(environmentPanel, env.Text);
    }

    public void addVocabulary(Word voc)
    {
        addButton(vocabularyPanel, voc.Text);
    }

    private void addButton(GameObject panel, string text)
    {
        BasePanelController ctrl = panel.GetComponent<BasePanelController>();
        ctrl.addButton(text);
        //ctrl.onAddButton();
    }



    // Use this for initialization
    void Start()
    {

        // register private helper as word button click handler
        BasePanelController cmdCtrl = commandPanel.GetComponent<BasePanelController>();
        cmdCtrl.setButtonClickHandler(new CommandPanelButtonListener());
        BasePanelController sysCtrl = subsystemPanel.GetComponent<BasePanelController>();
        sysCtrl.setButtonClickHandler(new CommandPanelButtonListener());
        BasePanelController envCtrl = environmentPanel.GetComponent<BasePanelController>();
        envCtrl.setButtonClickHandler(new CommandPanelButtonListener());
        BasePanelController vocCtrl = vocabularyPanel.GetComponent<BasePanelController>();
        vocCtrl.setButtonClickHandler(new CommandPanelButtonListener());

    }

    // Private listener implementations
    private class CommandPanelButtonListener : IButtonListClickListener{
    
         public void buttonClicked(string name){
             GameManager.Instance.WordClicked(WordFactory.getWord(name));
         }

    }
    private class SubsystemPanelButtonListener : IButtonListClickListener
    {

        public void buttonClicked(string name)
        {
            GameManager.Instance.WordClicked(WordFactory.getWord(name));
        }

    }
    private class EnvironmentPanelButtonListener : IButtonListClickListener
    {

        public void buttonClicked(string name)
        {
            GameManager.Instance.WordClicked(WordFactory.getWord(name));
        }

    }
    private class VocabularyPanelButtonListener : IButtonListClickListener
    {

        public void buttonClicked(string name)
        {
            GameManager.Instance.WordClicked(WordFactory.getWord(name));
        }

    }

    public void clearCommands()
    {
        ButtonListController ctrl = commandPanel.GetComponent<ButtonListController>();
        ctrl.removeAllButtons();
    }


}
