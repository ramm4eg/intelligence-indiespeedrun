﻿using UnityEngine;
using System.Collections;

public class Fader : MonoBehaviour {

    public void FadeOut(GameObject go)
    {
        CanvasGroup cg = go.GetComponent<CanvasGroup>();
        if (cg != null)
        {
            StartCoroutine(doFade(cg, true));
        }

    }

    public void FadeIn(GameObject go)
    {
        CanvasGroup cg = go.GetComponent<CanvasGroup>();
        if (cg != null)
        {
            StartCoroutine(doFade(cg, false));
        }

    }


    IEnumerator doFade(CanvasGroup cg, bool direction_out)
    {
        if (direction_out)
        {
            while (cg.alpha > 0)
            {
                cg.alpha -= Time.deltaTime / 2;
                yield return null;
            }

        }
        else
        {
            while (cg.alpha < 1)
            {
                cg.alpha += Time.deltaTime / 2;
                yield return null;
            }

        }


        //cg.interactable = false;
        yield return null;
    }
	

}
