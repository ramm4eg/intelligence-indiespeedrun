﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameManager : MonoBehaviour
{
    

    private static GameManager _instance;
    public static GameManager Instance { get { return _instance; } }
    private string _userName;
    public string UserName { get { return _userName; } internal set { _userName = value; } }
    public bool IsDataAvailable { get { return _isDataAvailable; } }


    private LinkedList<Word> line;
    private List<CommandListener> _commandListeners;
    private bool isProcessingEnter = false;

    private List<Subsystem> _subsystems;
    private List<Command> _commands;
    private List<Word> _vocabulary;

    private int _materials = 0;
    private bool _isDataAvailable = false;
    private LinkedList<Word> _lastData;
    private EnvObject _acquiredObject;
    private int _rating = 5;

    public AudioClip messageSound;
    public AudioClip buildSound;
    public AudioClip dataAcquiredSound;



    // Use this for initialization
    void Start()
    {
        if (_instance != null)
        {
            Destroy(gameObject);
            return;
        }

        _instance = this;
        line = new LinkedList<Word>();
        ConsoleController consoleController = GetComponent<ConsoleController>();
        consoleController.setPrefix(">>");

        _commandListeners = new List<CommandListener>();

        WordFactory.Init();

        _subsystems = new List<Subsystem>();
        _commands = new List<Command>();
        _vocabulary = new List<Word>();

        GameStateMachine.Init(consoleController);
    }

    internal void LearnWord(string wordText)
    {
        Word word = WordFactory.getWord(wordText);
        if (word is Command || word is Subsystem || word is EnvObject)
        {
            return;
        }
        if (_vocabulary.Contains(word))
        {
            return;
        }
        _vocabulary.Add(word);
        GetComponent<WordPanelController>().addVocabulary(word);
    }

    internal bool buildSubsystem(Subsystem subsys)
    {
        ConsoleController cc = GetComponent<ConsoleController>();
        if (_subsystems.Contains(subsys))
        {
            cc.ShowError("Module " + subsys.Text + " is already online.");
            return false;
        }
        else if (_materials < subsys.Price)
        {
            cc.ShowError("You need " + subsys.Price + " materials to build " + subsys.Text);
            return false;
        }
        else
        {
            updateMaterials(-subsys.Price);
            _subsystems.Add(subsys);
            List<Command> newCommands = subsys.getCommands();
            foreach (Command newCommand in newCommands)
            {
                addCommand(newCommand);
            }
            cc.ShowConfirmation(subsys.Text + " module is built succesfully.");

            SoundManager.instance.PlaySingle(buildSound);
            return true;
        }
    }

    internal void addCommand(Command newCommand)
    {
        if (!_commands.Contains(newCommand))
        {
            _commands.Add(newCommand);
            GetComponent<WordPanelController>().addCommand(newCommand);
        }
    }

    // Update is called once per frame
    void Update()
    {
        GameStateMachine.Update();
        if (Input.GetKeyUp(KeyCode.Return) || Input.GetKeyUp("enter"))
        {
            EnterClicked();
        }
    }

    public void WordClicked(Word word)
    {
        MainFocusController focusCtrl = GetComponent<MainFocusController>();
        focusCtrl.setExecuteFocus();
        line.AddLast(word);
        ConsoleController consoleController = GetComponent<ConsoleController>();
        consoleController.Type(word.Text + " ");
    }

    internal void addCommandListener(CommandListener listener)
    {
        _commandListeners.Add(listener);
    }

    internal void removeCommandListener(CommandListener listener)
    {
        _commandListeners.Remove(listener);
    }

    public void EnterClicked()
    {
        if (!isProcessingEnter)
        {
            StartCoroutine(ProcessEnter());
        }

    }

    IEnumerator ProcessEnter()
    {
        isProcessingEnter = true;
        ConsoleController cc = GetComponent<ConsoleController>();
        if (cc.isTyping())
        {
            cc.accelerate();
        }
        else
        {
            if (line.First != null)
            {
                executeLine(line);
                line.Clear();
            }
            GetComponent<ConsoleController>().TypeLine("");
        }
        yield return new WaitForSeconds(0.2f);
        isProcessingEnter = false;
    }

    private void executeLine(LinkedList<Word> line)
    {
        // evaluate command
        Word word = line.First.Value;
        Command cmd = word as Command;
        if (cmd == null)
        {
            ConsoleController consoleController = GetComponent<ConsoleController>();
            consoleController.ShowError(word.Text + " is not a command");
            return;
        }

        bool success = cmd.execute(line);
        if (success)
        {
            notifyCommandListeners(line);
        }
    }

    private void notifyCommandListeners(LinkedList<Word> line)
    {
        foreach (CommandListener listener in _commandListeners)
        {
            listener.commandExecuted(line);
        }
    }

    internal interface CommandListener
    {
        void commandExecuted(LinkedList<Word> line);
    }

    internal void initMaterials()
    {
        _materials = 10;
        updateMaterialsDisplay();
    }

    internal void updateMaterials(int change)
    {
        _materials += change;
        updateMaterialsDisplay();
    }

    private void updateMaterialsDisplay()
    {
        WordPanelController wpc = GetComponent<WordPanelController>();
        GameObject panel = wpc.materialsPanel;
        MaterialsPanelController contr = panel.GetComponent<MaterialsPanelController>();
        string v = "Materials:" + _materials.ToString();
        contr.setMaterialsText(v);
    }

    private void updateDataDisplay()
    {
        WordPanelController wpc = GetComponent<WordPanelController>();
        GameObject panel = wpc.materialsPanel;
        MaterialsPanelController contr = panel.GetComponent<MaterialsPanelController>();
        string v = "No Data Acquired";
        if (_isDataAvailable)
        {
            v = "New Data Available";
        }
        contr.setDataText(v);
    }

    internal void acquireData(Command cmd, EnvObject target)
    {
        ConsoleController cc = GetComponent<ConsoleController>();
        if (_isDataAvailable)
        {
            cc.ShowError("Data already acquired. Please send to clear data cache.");
        }
        else
        {
            if (cmd.Text == "scan_space")
            {
                target = WordFactory.getWord("space") as EnvObject;
            }

            //Debug.Log(target.Text);

            if (target != null)
            {

                List<EnvObject> result = target.analyse(cmd);
                if (result != null && result.Count > 0)
                {
                    _acquiredObject = result.ToArray()[0];
                    //Debug.Log(_acquiredObject.Text);

                    result.Remove(_acquiredObject);


                }
                else
                {
                    _acquiredObject = null;
                }

                _isDataAvailable = true;

                updateDataDisplay();

                cc.ShowConfirmation("New Data Acquired.");
                SoundManager.instance.PlaySingle(dataAcquiredSound);
            }
            else
            {
                cc.ShowError("Could not identify target.");
            }
        }
    }

    internal void sendData(LinkedList<Word> line)
    {

        ConsoleController cc = GetComponent<ConsoleController>();
        if (!_isDataAvailable)
        {
            cc.ShowError("No Data available.");
        }
        else
        {
            updateDataDisplay();

            string s = "Sending Data with Message: ";

            if (line == null || line.Count <= 1)
            {
                s += "<Empty>";
            }
            else
            {
                var it = line.First.Next;
                while (it != null)
                {
                    s += it.Value.Text;
                    s += " ";
                    it = it.Next;
                }
            }

            cc.ShowConfirmation(s);

            calculateResponse();

        }
    }

    private void calculateResponse()
    {
        _isDataAvailable = false;
        updateDataDisplay();

        ConsoleController cc = GetComponent<ConsoleController>();

        cc.ShowWarning("Data Received.");
        if (_acquiredObject != null)
        {
            cc.ShowWarning("New Environment Object: " + _acquiredObject.Text);
            GetComponent<WordPanelController>().addEnvironment(_acquiredObject);

            if (_acquiredObject.Text.StartsWith("star"))
            {
                cc.ShowWarning("Objective: Search for Planets.");
            }
            if (_acquiredObject.Text.StartsWith("planet"))
            {
                cc.ShowWarning("Objective: Search for Life.");
            }
            if (_acquiredObject.Text.StartsWith("lifeform"))
            {
                cc.ShowWarning("Objective: Leave in Peace");
            }
            _rating++;

        }
        else
        {
            cc.ShowError("No new Objects found.");
            _rating--;
        }
        cc.ShowWarning("Mission Rating: ");
        int nuMat = 0;
        if (_rating >= 5)
        {
            cc.ShowConfirmation("Success");
            nuMat = 20;
        }
        else if (_rating >= 2)
        {
            cc.ShowWarning("Stagnating. If no new objects are found, the mission will be aborted.");
            nuMat = 15;
        }
        else if (_rating > 0)
        {
            cc.ShowError("Useless. Abort imminent.");
            nuMat = 5;
        }
        else
        {
            cc.ShowError("Failure. Stopping communication.");
            cc.ShowError("Failure. Stopping communication.");
            cc.ShowError("Failure. Stopping communication.");
            cc.ShowError("Failure. Stopping communication.");
            cc.ShowError("Failure. Stopping com");
            // You Lose :-(
            GetComponent<MainFocusController>().ShowYouLose();


            nuMat = 0;
        }

        cc.ShowWarning(nuMat + " Material Received");
        updateMaterials(nuMat);
        //cc.ShowWarning("Message: Here it comes");

    }


    internal void sendResponceMessage(string v)
    {
        string message = "Message: " + v;
        GetComponent<ConsoleController>().ShowWarning(message);
        SoundManager.instance.PlaySingle(messageSound);
    }


}
