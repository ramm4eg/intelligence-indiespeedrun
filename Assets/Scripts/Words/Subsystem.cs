﻿using System;
using System.Collections.Generic;

internal abstract class Subsystem : Word
{
    protected List<Command> _commands = new List<Command>();

    public int Price { get; internal set; }

    internal List<Command> getCommands()
    {
        return _commands;
    }
}