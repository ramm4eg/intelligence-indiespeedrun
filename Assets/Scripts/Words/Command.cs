﻿using System;
using System.Collections.Generic;

internal abstract class Command : Word
{
    protected string _usage;
    internal abstract bool execute(LinkedList<Word> line);
}