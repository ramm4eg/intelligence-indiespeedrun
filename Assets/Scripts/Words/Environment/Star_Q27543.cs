﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

internal class Star_Q27543 : EnvObject
{

    List<EnvObject> planetList;


    public Star_Q27543()
    {
        _text = "star_Q27543";
        planetList = new List<EnvObject>();
        planetList.Add(WordFactory.getWord("planet_Q27543_1") as EnvObject);
        _subEnv.Add(WordFactory.getWord("ir_scan") as Command, planetList);

    }
	
}
