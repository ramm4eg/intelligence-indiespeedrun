﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

internal class Planet_Q27543_1 : EnvObject
{

    List<EnvObject> alienList
        ;


    public Planet_Q27543_1()
    {
        _text = "planet_Q27543_1";
        alienList = new List<EnvObject>();
        alienList.Add(WordFactory.getWord("lifeform_Q27543_1") as EnvObject);

        _subEnv.Add(WordFactory.getWord("send_bio_probe") as Command, alienList);
            
    }
	
}
