﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

internal class SpaceEnvironment : EnvObject
{

    List<EnvObject> sunList;


    public SpaceEnvironment()
    {
        _text = "space";
        sunList = new List<EnvObject>();
        sunList.Add(WordFactory.getWord("star_Q27543") as EnvObject);
        _subEnv.Add(WordFactory.getWord("scan_space") as Command, sunList);
        
    }
	
}
