﻿internal class BioProbe : Subsystem
{
    public BioProbe()
    {
        _text = "bio_probe";

        Command cmd = WordFactory.getWord("send_bio_probe") as Command;

        
            _commands.Add(cmd);
        
        Price = 15;
    }
}