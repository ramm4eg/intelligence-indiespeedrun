﻿using System.Collections.Generic;

internal class Telescope : Subsystem
{
    public Telescope()
    {
        _text = "telescope";
        _commands.Add(WordFactory.getWord("scan_space") as Command);
        Price = 5;
    }
}