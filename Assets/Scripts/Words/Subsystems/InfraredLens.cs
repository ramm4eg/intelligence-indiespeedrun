﻿internal class InfraredLens : Subsystem
{
    public InfraredLens()
    {
        _text = "ir_lens";

        Command cmd = WordFactory.getWord("ir_scan") as Command;
        _commands.Add(cmd);

        Price = 15;
    }
}