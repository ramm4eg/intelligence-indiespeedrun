﻿internal class MagneticProbe : Subsystem
{
    public MagneticProbe()
    {
        _text = "magnetic_probe";

        Command cmd = WordFactory.getWord("send_magnetic_probe") as Command;

        
            _commands.Add(cmd);
        
        Price = 15;
    }
}