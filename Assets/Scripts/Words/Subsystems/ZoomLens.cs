﻿internal class ZoomLens : Subsystem
{
    public ZoomLens()
    {
        _text = "zoom_lens";

        Command cmd = WordFactory.getWord("zoom_scan") as Command;

        
            _commands.Add(cmd);
        
        Price = 15;
    }
}