﻿internal class Communicator : Subsystem
{
    public Communicator()
    {
        _text = "communicator";
        _commands.Add(WordFactory.getWord("send_data") as Command);
        Price = 5;
    }
}