﻿using System;
using System.Collections.Generic;

internal class WordFactory
{
    private static Dictionary<string, Word> _words;

    internal static void Init()
    {
        _words = new Dictionary<string, Word>();
        
        // commands before start
        _words.Add("start_game", new StartCommand());
        _words.Add("controls", new ControlsCommand());
        _words.Add("description", new DescriptionCommand());
        _words.Add("better_description", new BetterDescriptionCommand());
        _words.Add("thanks", new ThanksCommand());

        // game commands
        _words.Add("build", new BuildCommand());
        _words.Add("scan_space", new ScanSpaceCommand());
        _words.Add("ir_scan", new IRScanCommand());
        _words.Add("uv_scan", new UVScanCommand());
        _words.Add("zoom_scan", new ZoomScanCommand());
        _words.Add("send_bio_probe", new SendBioProbeCommand());
        _words.Add("send_magnetic_probe", new SendMagneticProbeCommand());
        _words.Add("send_data", new SendDataCommand());
        _words.Add("send_message", new SendMessageCommand());

        // subsystems
        _words.Add("telescope", new Telescope());
        _words.Add("communicator", new Communicator());
        _words.Add("ir_lens", new InfraredLens());
        _words.Add("uv_lens", new UVLens());
        _words.Add("zoom_lens", new ZoomLens());
        _words.Add("bio_probe", new BioProbe());
        _words.Add("magnetic_probe", new MagneticProbe());
        _words.Add("communicator_2.0", new Communicator2());
        
        // environment, order is crucial, because constructors will use this dictionary!!
        _words.Add("lifeform_Q27543_1", new Alien_Q27543_1());
        _words.Add("planet_Q27543_1", new Planet_Q27543_1());
        _words.Add("star_Q27543", new Star_Q27543());
        _words.Add("space", new SpaceEnvironment());
        _words.Add("nodata", new NoData());
    }

    internal static Word getWord(string wordText)
    {
        Word word;
        bool found = _words.TryGetValue(wordText, out word);
        if (found)
        {
            return word;
        }

        // create new word
        word = new GenericWord(wordText);
        _words.Add(wordText, word);

        return word;
    }

    private class GenericWord : Word
    {
        public GenericWord(string wordText)
        {
            _text = wordText;
        }
    }
}