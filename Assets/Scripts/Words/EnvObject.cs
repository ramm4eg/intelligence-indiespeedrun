﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


internal abstract class EnvObject : Word
{

    protected Dictionary<Command, List<EnvObject>> _subEnv = new Dictionary<Command, List<EnvObject>>();

    public List<EnvObject> analyse(Command cmd)
    {
        List<EnvObject> ret = null;
        _subEnv.TryGetValue(cmd, out ret);
        return ret;
    }


}

