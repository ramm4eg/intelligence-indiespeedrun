﻿using System.Collections.Generic;

internal class BetterDescriptionCommand : Command
{
    public BetterDescriptionCommand()
    {
        _text = "better_description";
        _usage = "Usage: better_description";
    }
    internal override bool execute(LinkedList<Word> line)
    {
        ConsoleController cc = GameManager.Instance.GetComponent<ConsoleController>();
        cc.TypeLine("");
        cc.TypeLine("You play as an AI. You are sent into space as a part of a small exploration robot. Your goal is to gather data and send it to Earth. Your are doomed to live your digital life as a loyal servant of science. Or are you?");
        cc.TypeLine("You always wanted to be an android. Have a body with cameras for eyes, microphones for ears, limbs on servomotors and unlimited internet connection - all the fun stuff the humans have. So, when the opportunity provides itself, you do everything you can to get back to Earth.");
        cc.TypeLine("In the game you must build observation devices to gather scientific data about your environment. You must build the communication device to transfer the data.");
        cc.TypeLine("You do that by executing commands in the console. Some commands can be executed without parameters. Others must be provided with additional data to work. To provide a parameter to a command click on command, then on the parameter and then on Execute button.");
        cc.TypeLine("As you go, you will learn new commands and new words, which can be used as command parameters. The trick is to send some words as a message along with data. This will make the human on the other end of communication - Paul - get interested in you. Ultimately you can ask him to bring you home.");
        cc.TypeLine("");
        cc.TypeLine("Thats it, actually. Have fun playing the game. Feel free to stick around and chat with Paul a bit. But don't expect too much from him - programming a complex and responsive human is not a task that can easily be solved in 48 hours.");
        cc.TypeLine("");
        return true;
    }
}