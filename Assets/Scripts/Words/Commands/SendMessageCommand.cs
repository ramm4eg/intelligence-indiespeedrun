﻿using System;
using System.Collections.Generic;
using UnityEngine;


class SendMessageCommand : Command
{

    public SendMessageCommand()
    {
        _text = "send_message";
        _usage = "Usage: send_message [message]";
    }

    internal override bool execute(LinkedList<Word> line)
    {
        ConsoleController cc = GameManager.Instance.GetComponent<ConsoleController>();
        if (line.First == null || line.First.Next == null)
        {
            cc.ShowError(_usage);
            return false;
        }
        else
        {
            // no need to do anything here
            // the evaluation of the message will be done by current state
            return true;
        }
    }
    
}

