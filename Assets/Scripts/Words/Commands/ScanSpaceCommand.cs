﻿using System;
using System.Collections.Generic;
using UnityEngine;

internal class ScanSpaceCommand : Command
{
    public ScanSpaceCommand()
    {
        _text = "scan_space";
        _usage = "Usage: scan_space";
    }

    internal override bool execute(LinkedList<Word> line)
    {
        //line.RemoveFirst(); // remove the command itself

        //ConsoleController cc = GameManager.Instance.GetComponent<ConsoleController>();

        //if (line.First != null)
        //{
        //    cc.ShowError(_usage);
        //}
        //else
        //{
        GameManager.Instance.acquireData(this, null);
        return true;

        //}
    }
}