﻿using System.Collections.Generic;

internal class StartCommand : Command
{
    public StartCommand()
    {
        _text = "start_game";
    }
    internal override bool execute(LinkedList<Word> line)
    {
        // no need to do anything
        return true;
    }
}