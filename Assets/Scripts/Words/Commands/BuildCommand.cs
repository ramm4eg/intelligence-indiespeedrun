﻿using System;
using System.Collections.Generic;
using UnityEngine;

internal class BuildCommand : Command
{
    public BuildCommand()
    {
        _text = "build";
        _usage = "Usage: build [subsystem] - click on 'build' command, click on subsystem you want to build and then click Execute button";
    }
    internal override bool execute(LinkedList<Word> line)
    {
        
        ConsoleController cc = GameManager.Instance.GetComponent<ConsoleController>();
        LinkedListNode<Word> second = line.First.Next;
        if (second == null)
        {
            cc.ShowError(_usage);
            return false;
        }
        else
        {
            Subsystem subsys = second.Value as Subsystem;
            if (subsys == null)
            {
                cc.ShowError(_usage);
                return false;
            }
            else
            {
                return GameManager.Instance.buildSubsystem(subsys);
            }
        }
    }
}