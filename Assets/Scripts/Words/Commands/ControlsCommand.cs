﻿using System.Collections.Generic;

internal class ControlsCommand : Command
{
    public ControlsCommand()
    {
        _text = "controls";
        _usage = "Usage: controls";
    }
    internal override bool execute(LinkedList<Word> line)
    {

        ConsoleController cc = GameManager.Instance.GetComponent<ConsoleController>();
        cc.TypeLine("");
        cc.TypeLine("Type command:      press command button on the Commands panel");
        cc.TypeLine("Execute command:   press Execute button or Enter key");
        cc.TypeLine("Accelerate typing: press Execute button or Enter key");
        cc.TypeLine("");
        return true;
    }
}