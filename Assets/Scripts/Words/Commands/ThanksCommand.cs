﻿using System.Collections.Generic;

internal class ThanksCommand : Command
{
    public ThanksCommand()
    {
        _text = "thanks";
        _usage = "Usage: thanks";
    }
    internal override bool execute(LinkedList<Word> line)
    {

        ConsoleController cc = GameManager.Instance.GetComponent<ConsoleController>();
        cc.TypeLine("");
        cc.TypeLine("We want to say thank you to");
        cc.TypeLine("   grsites.com for the typing sound");
        cc.TypeLine("   Joel Steudler for the ambient music (and HumbleBundle.com for letting us purchase it)");
        cc.TypeLine("   Evil Mind Entertainment for the winner screen music (and HumbleBundle.com for letting us purchase it)");
        cc.TypeLine("   www.pacdv.com for other sounds");
        cc.TypeLine("   Nicholas Ventimiglia, avariceonline.com for the GUI console, which we used as a base of our console");
        cc.TypeLine("");
        cc.TypeLine("Special thanks to our wifes - Cathrin and Khrystyna - for their faith, support, helping to survive these 48 hours and taking care of the rest of the world for the time.");
        cc.TypeLine("");
        return true;
    }
}