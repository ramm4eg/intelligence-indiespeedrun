﻿using System;
using System.Collections.Generic;
using UnityEngine;

internal class IRScanCommand : Command
{
    public IRScanCommand()
    {
        _text = "ir_scan";
        _usage = "Usage: ir_scan [target_planet]";
    }

    internal override bool execute(LinkedList<Word> line)
    {
        ConsoleController cc = GameManager.Instance.GetComponent<ConsoleController>();

        if (line.First == null || line.First.Next == null)
        {
            cc.ShowError(_usage);
            return false;
        }
        else
        {
            EnvObject target = line.First.Next.Value as EnvObject;

            if (target == null)
            {
                cc.ShowError(_usage);
                return false;
            }
            else
            {
                GameManager.Instance.acquireData(this, target);
                return true;
            }
        }
    }
}