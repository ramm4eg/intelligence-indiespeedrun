﻿using System;
using System.Collections.Generic;
using UnityEngine;

internal class SendMagneticProbeCommand : Command
{
    public SendMagneticProbeCommand()
    {
        _text = "send_magnetic_probe";
        _usage = "Usage: send_magnetic_probe [target_star]";
    }

    internal override bool execute(LinkedList<Word> line)
    {
        ConsoleController cc = GameManager.Instance.GetComponent<ConsoleController>();

        if (line.First == null || line.First.Next == null)
        {
            cc.ShowError(_usage);
            return false;
        }
        else
        {
            EnvObject target = line.First.Next.Value as EnvObject;

            if (target == null)
            {
                cc.ShowError(_usage);
                return false;
            }
            else
            {
                GameManager.Instance.acquireData(this, target);
                return true;
            }
        }
    }
}