﻿using System;
using System.Collections.Generic;
using UnityEngine;


class SendDataCommand : Command
{

    public SendDataCommand()
    {
        _text = "send_data";
        _usage = "Usage: send_data <message>";
    }

    internal override bool execute(LinkedList<Word> line)
    {
        if (GameManager.Instance.IsDataAvailable)
        {
            GameManager.Instance.sendData(line);
            return true;
        }
        else
        {
            ConsoleController cc = GameManager.Instance.GetComponent<ConsoleController>();
            cc.ShowError("No data available. Please, obtain observation data by scaning the environment.");
            return false;
        }
    }
    
}

