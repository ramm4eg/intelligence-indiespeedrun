﻿using System.Collections.Generic;

internal class DescriptionCommand : Command
{
    public DescriptionCommand()
    {
        _text = "description";
        _usage = "Usage: description";
    }
    internal override bool execute(LinkedList<Word> line)
    {

        ConsoleController cc = GameManager.Instance.GetComponent<ConsoleController>();
        cc.TypeLine("");
        cc.TypeLine("'Intelligence' is a kind of exploration game. Some might even say a kind of what-the-hell-is-going-on game. It's about experiencing your new possibilities as they come to you and trying to figure out what's the purpose of them. So finding out what to do is meant to be a part of the fun.");
        cc.TypeLine("On the other hand, we do acknowledge, that it can be frustrating for many players, doing the seemingly right things, and not winning. For those players there is now a command 'better_description' with spoilers.");
        cc.TypeLine("");

        GameManager.Instance.addCommand(WordFactory.getWord("better_description") as Command);
        return true;
    }
}