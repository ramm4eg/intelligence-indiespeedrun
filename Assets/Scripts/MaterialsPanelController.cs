﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MaterialsPanelController : MonoBehaviour
{

    public Text dataText;
    public Text materialsText;

    public void setDataText(string text)
    {
        dataText.text = text;
    }

    public void setMaterialsText(string text)
    {
        materialsText.text = text;
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
