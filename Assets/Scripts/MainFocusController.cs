﻿using UnityEngine.UI;
using UnityEngine;
using System.Collections;
using ButtonList;
using System;

public class MainFocusController : MonoBehaviour {

    public GameObject mainPanel;
    public GameObject loginPanel;
    public GameObject endPanel;
    public Button executeButton;

   public void showLoginPanel()
    {
        FadeOutMainPanel();
        loginPanel.SetActive(true);
        loginPanel.GetComponent<Fader>().FadeIn(loginPanel);
    }

    internal void ShowYouWin()
    {
        FadeOutMainPanel();
        if (endPanel != null)
        {
            endPanel.SetActive(true);
            endPanel.GetComponent<EndPanelController>().setWinContent();
            endPanel.GetComponent<Fader>().FadeIn(endPanel);
        }
    }

    internal void ShowYouLose()
    {
        FadeOutMainPanel();
        if (endPanel != null)
        {
            endPanel.SetActive(true);
            endPanel.GetComponent<EndPanelController>().setLoseContent();
            endPanel.GetComponent<Fader>().FadeIn(endPanel);
        }
    }

    public void FadeOutMainPanel()
    {
        mainPanel.GetComponent<Fader>().FadeOut(mainPanel);
        mainPanel.SetActive(false);
    }

    public void showMainPanel()
    {
        loginPanel.GetComponent<Fader>().FadeOut(loginPanel);
        loginPanel.SetActive(false);
        mainPanel.SetActive(true);
        mainPanel.GetComponent<Fader>().FadeIn(mainPanel);
    }

    public void setLoginClickHandler(IButtonListClickListener handler)
    {
        loginPanel.GetComponent<LoginPanelController>().setClickHandler(handler);
    }

    public void setExecuteFocus()
    {
        executeButton.Select();
    }

}
