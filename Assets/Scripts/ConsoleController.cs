﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class ConsoleController : MonoBehaviour
{
    // Audio
    public AudioClip typeSound1;

    private string _prefix = "";
    LinkedList<string> textsToType;

    const string PREFIX_TYPE_COMMAND = "_<>prefix>_";
    private bool _typing = false;
    private string _textBeingTyped = "";
    private int _typingIndex = 0;
    const float TYPING_INTERVAL = 0.04f;
    float _typingTimer = 0f;

    bool _underscoreSet = false;
    const float UNDERSCORE_INTERVAL = 0.5f;
    float _underscoreTimer = 0f;

    public AudioClip errorSound;

    internal bool isTyping()
    {
        return _typing;
    }

    // Use this for initialization
    void Start()
    {
        textsToType = new LinkedList<string>();
        GUIConsole.ConsoleContext.Instance.LogText(_prefix);
    }

    internal void ShowConfirmation(string text)
    {
        if (_underscoreSet)
        {
            toggleUnderscore(false);
        }
        GUIConsole.ConsoleContext.Instance.LogSuccess(text);
    }

    // Update is called once per frame
    void Update()
    {
        if (_typing)
        {
            typeNextLetter();
            SoundManager.instance.StartLoop(typeSound1);
        }
        else if (textsToType.First != null)
        {
            string toType = textsToType.First.Value;
            if (PREFIX_TYPE_COMMAND.Equals(toType))
            {
                textsToType.RemoveFirst();
                GUIConsole.ConsoleContext.Instance.LogText(_prefix);
            }
            else
            {
                textsToType.RemoveFirst();
                startTyping(toType);
            }
        }
        else
        {
            // nothing to type
            toggleUnderscore();
            SoundManager.instance.StopLoop();
        }

    }

    private void toggleUnderscore()
    {
        if (_underscoreTimer < UNDERSCORE_INTERVAL)
        {
            _underscoreTimer += Time.deltaTime;
        }
        else
        {
            toggleUnderscore(!_underscoreSet);
        }
    }

    private void startTyping(string toType)
    {
        
        _typing = true;
        _typingIndex = 0;
        _textBeingTyped = toType;

        if (_underscoreSet)
        {
            toggleUnderscore(false);
        }
    }

    internal void accelerate()
    {
        if (!_typing)
        {
            return;
        }
        
        GUIConsole.ConsoleContext.Instance.ExtendLast(_textBeingTyped.Substring(_typingIndex), true);
        _typingTimer = 0;
        _typingIndex = 0;
        _typing = false;
        _textBeingTyped = "";
    }

    private void toggleUnderscore(bool set)
    {
        if (set)
            GUIConsole.ConsoleContext.Instance.ExtendLast("_", false);
        else
            GUIConsole.ConsoleContext.Instance.RemoveLastChar();
        _underscoreSet = set;
        _underscoreTimer = 0;
    }

    internal void ShowError(string text)
    {
        if (_underscoreSet)
        {
            toggleUnderscore(false);
        }
        GUIConsole.ConsoleContext.Instance.LogError(text);
        SoundManager.instance.PlaySingle(errorSound);
    }

    internal void ShowWarning(string text)
    {
        if (_underscoreSet)
        {
            toggleUnderscore(false);
        }
        GUIConsole.ConsoleContext.Instance.LogWarning(text);
    }

    private void typeNextLetter()
    {
        if (_textBeingTyped.Length <= _typingIndex)
        {
            _typingTimer = 0;
            _typing = false;
            _textBeingTyped = "";
            return;
        }

        if (_typingTimer < TYPING_INTERVAL)
        {
            _typingTimer += Time.deltaTime;
        }
        else
        {
            char letter = _textBeingTyped[_typingIndex];
            GUIConsole.ConsoleContext.Instance.ExtendLast(letter.ToString(), true);
            _typingTimer = 0;
            _typingIndex++;
        }
    }

    internal void setPrefix(string prefix)
    {
        _prefix = prefix;
    }

    internal void TypeLine(string text)
    {
        textsToType.AddLast(text);
        textsToType.AddLast(PREFIX_TYPE_COMMAND);
    }

    internal void Type(string text)
    {
        textsToType.AddLast(text);
    }
}
