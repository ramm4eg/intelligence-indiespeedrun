﻿
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using ButtonList;


public abstract class BasePanelController : MonoBehaviour, IButtonListClickListener
{


    private ButtonListController buttonController;
    private IButtonListClickListener buttonClickHandler;

    private LinkedList<string> createButtons = new LinkedList<string>();
    private bool _isCreatingButton = false;

    // Use this for initialization
    void Start()
    {

        buttonController = GetComponent<ButtonListController>();
        buttonController.addClickListener(this);

        }

    // Update is called once per frame
    void Update()
    {
        if (createButtons.Count > 0)
        {
            if (!_isCreatingButton)
            {
                StartCoroutine(createButton());
            }
        }
    }

    System.Collections.IEnumerator createButton()
    {
        _isCreatingButton = true;

        buttonController.addButton(createButtons.First.Value);
        createButtons.RemoveFirst();
        
        yield return new WaitForSeconds(0.5f);
        _isCreatingButton = false;
    }

    public void setButtonClickHandler(IButtonListClickListener handler)
    {
        buttonClickHandler = handler;
    }


    public void onAddButton()
    {

        // Create text of random length

        string bText = "";
        int length = Random.Range(5, 45);
        string characters = "0123456789abcdefghijklmnopqrstuvwxABCDEFGHIJKLMNOPQRSTUVWXYZ";

        for (int i = 0; i < length; i++)
        {
            int a = Random.Range(0, characters.Length);
            bText += characters[a];
        }

        buttonController.addButton(bText);

    }

    public void addButton(string name)
    {
        createButtons.AddLast(name);
    }

    public void onClearButton(){
        buttonController.removeAllButtons();
    }





    public void buttonClicked(string name)
    {
        
        if (buttonClickHandler != null)
        {
            buttonClickHandler.buttonClicked(name);
        }
    }
    


}

