﻿using UnityEngine;
using System.Collections;

public class SystemPanelController : BasePanelController
{

    public new void buttonClicked(string name)
    {
        Debug.Log("System Panel: " + name);
        base.buttonClicked(name);
    }
}
